package com.captainpirate.tutorialmod.util;

public interface IHasModel 
{
	public void registerModels();
}
