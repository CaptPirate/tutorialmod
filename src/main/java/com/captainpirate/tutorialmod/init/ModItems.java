package com.captainpirate.tutorialmod.init;

import java.util.ArrayList;
import java.util.List;

import com.captainpirate.tutorialmod.items.ItemBase;

import net.minecraft.item.Item;

public class ModItems 
{

	public static final List<Item> ITEMS = new ArrayList<Item>();
	
	public static final Item RUBY = new ItemBase("ruby");
	public static final Item CAT = new ItemBase("cat");
	public static final Item CAPTAIN = new ItemBase("captain");
	
}
